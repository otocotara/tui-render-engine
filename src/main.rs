mod extra;
mod screen;
mod shape;

use extra::*;
use screen::*;
use shape::*;

fn main() {
    let face = Face::new(' ', '|', '-', ['+', '+', '+', '+']);
    let mut screen = Screen::new(50, 50, face);
    let mut square = Shape::new_scale(
        Vector3D::new(10, 10, 10),
        vec![
            Vector3D::new(00, 00, 0),
            Vector3D::new(10, 00, 0),
            Vector3D::new(10, 10, 0),
            Vector3D::new(00, 10, 0),
            Vector3D::new(05, 05, 1),
            Vector3D::new(15, 05, 1),
            Vector3D::new(15, 15, 1),
            Vector3D::new(05, 15, 1),
        ],
        vec![
            (0, 1),
            (1, 2),
            (2, 3),
            (3, 0),
            (0, 4),
            (1, 5),
            (2, 6),
            (3, 7),
            (4, 5),
            (5, 6),
            (6, 7),
            (7, 4),
        ],
    );

    let mut frame = 0;

    loop {
        frame += 1;
        screen.clear();

        if frame % 100 == 0 {
            square.mv(1, 0, 0);
        }

        screen.draw_shape(&square);
        println!("{}", screen.get_buffer_str());
    }
}
