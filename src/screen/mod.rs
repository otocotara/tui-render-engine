use crate::extra::*;
use crate::shape::*;

pub struct Screen {
    w: usize,
    h: usize,
    chars: Vec<char>,
    face: Face,
}

impl Screen {
    pub fn new(w: usize, h: usize, face: Face) -> Self {
        Self {
            w,
            h,
            chars: vec![face.background; w * h],
            face,
        }
    }

    pub fn draw_shape(&mut self, shape: &Shape) {
        for edge in &shape.edges {
            let v1 = &shape.vertexes[edge.0];
            let v2 = &shape.vertexes[edge.1];

            self.draw_line(v1.x, v1.y, v2.x, v2.y);
        }

        for vertex in &shape.vertexes {
            self.draw_point(vertex.x, vertex.y, '@');
        }
    }

    pub fn clear(&mut self) {
        self.chars.fill(self.face.background);
    }

    pub fn get_buffer_str(&self) -> String {
        let mut buffer = String::with_capacity((self.w + 1) * self.h);

        for i in 0..self.chars.len() {
            let (x, _) = self.from_index(i);

            if x == 0 {
                buffer.push('\n');
            }

            buffer.push(self.chars[i]);
        }

        return buffer;
    }

    fn from_index(&self, i: usize) -> (usize, usize) {
        (i % self.w, i / self.w)
    }

    fn to_index(&self, x: usize, y: usize) -> usize {
        y * self.w + x
    }

    fn draw_point(&mut self, x: isize, y: isize, ch: char) {
        if x < 0 || x > self.w as isize || y < 0 || y > self.h as isize {
            return;
        }

        let i = self.to_index(x as usize, y as usize);
        self.chars[i] = ch;
    }

    fn draw_line(&mut self, x0: isize, y0: isize, x1: isize, y1: isize) {
        let dx = x1.abs_diff(x0) as isize;
        let dy = -(y1.abs_diff(y0) as isize);
        let sx = if x0 < x1 { 1 } else { -1 };
        let sy = if y0 < y1 { 1 } else { -1 };

        let mut err = dx + dy;
        let mut x = x0 as isize;
        let mut y = y0 as isize;
        let mut xx = x;
        let mut yy = y;

        loop {
            let e2 = 2 * err;
            let mut ch = self.face.horizontal;

            if x == xx && (y == yy - 1 || y == yy + 1) {
                ch = self.face.vertical;
            }

            self.draw_point(x, y, ch);

            xx = x;
            yy = y;

            if x == x1 && y == y1 {
                break;
            }
            if e2 >= dy {
                err += dy;
                x += sx;
            }
            if e2 <= dx {
                err += dx;
                y += sy;
            }
        }
    }
}
