use crate::extra::*;

pub struct Shape {
    pub vertexes: Vec<Vector3D>,
    pub edges: Vec<(usize, usize)>,
}

impl Shape {
    pub fn new_scale(pos: Vector3D, vertexes: Vec<Vector3D>, edges: Vec<(usize, usize)>) -> Self {
        let vertexes = vertexes.into_iter().map(|v| v.add(&pos)).collect();

        Self { vertexes, edges }
    }

    pub fn mv(&mut self, x: isize, y: isize, z: isize) {
        let vect = Vector3D::new(x, y, z);

        for vert in &mut self.vertexes {
            vert.add_self(&vect);
        }
    }
}
