pub struct Vector3D {
    x: usize,
    y: usize,
    z: usize,
}

pub struct Face {
    background: char,
    vertical: char,
    horizontal: char,
    corners: [char; 4],
}

pub struct Matrix {}

impl Vector3D {
    pub fn new(x: usize, y: usize, z: usize) -> Self {
        Self { x, y, z }
    }

    pub fn add(&self, vec: &Vector3D) -> Vector3D {
        Vector3D {
            x: self.x + vec.x,
            y: self.y + vec.y,
            z: self.z + vec.z,
        }
    }
}

impl Face {
    pub fn new(background: char, vertical: char, horizontal: char, corners: [char; 4]) -> Self {
        Self {
            background,
            vertical,
            horizontal,
            corners,
        }
    }
}
