pub struct Vector3D {
    pub x: isize,
    pub y: isize,
    pub z: isize,
}

pub struct Face {
    pub background: char,
    pub vertical: char,
    pub horizontal: char,
    pub corners: [char; 4],
}

pub struct Matrix {}

impl Vector3D {
    pub fn new(x: isize, y: isize, z: isize) -> Self {
        Self { x, y, z }
    }

    pub fn add(&self, vec: &Vector3D) -> Vector3D {
        Vector3D {
            x: self.x + vec.x,
            y: self.y + vec.y,
            z: self.z + vec.z,
        }
    }

    pub fn add_self(&mut self, vec: &Vector3D) {
        self.x += vec.x;
        self.y += vec.y;
        self.z += vec.z;
    }
}

impl Face {
    pub fn new(background: char, vertical: char, horizontal: char, corners: [char; 4]) -> Self {
        Self {
            background,
            vertical,
            horizontal,
            corners,
        }
    }
}
